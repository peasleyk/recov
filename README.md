# Recov

## What is this

This is a multi threaded script to downland images from reddit subreddts, primarily for datasets for machine learning. It's limited to 1000 imagesdue to API restrictions.

## Setting up

Install dependences with the pipfile
```
pipenv install
pipenv shell
```

Edit config.yml.example with a reddit api key and account details, remove .example

## How to use

Ideally I should probably split this up into a library and a cli program, but for now,

```
usage: redditset.py [-h] [-t] [-l LIMIT] [-f FOLDER]
                    [-s SUBREDDITS [SUBREDDITS ...]] [-k] [-m MAXTHREADS]
                    [-st]

optional arguments:
  -h, --help            show this help message and exit
  -t, --test            Tests your reddit connection
  -l LIMIT, --limit LIMIT
                        The number of reddit posts to download
  -f FOLDER, --folder FOLDER
                        folder to download images
  -s SUBREDDITS [SUBREDDITS ...], --subreddits SUBREDDITS [SUBREDDITS ...]
                        list of subreddits to download from
  -k, --keep            Places each posts media in a folder with the
                        submission name
  -m MAXTHREADS, --maxthreads MAXTHREADS
                        Number of threads to use, images to download at the
                        same timeand more dropped connections
  -st, --static         Only download static images
```

Albums from imgur are processed and also downloaded. Without ```--keep```, the album order is lost. With ```--keep```, Each album image will be renamed the post title like non album downloads, and will also have a number indicator for order in the album.

Setting ```--maxthreads``` too high may choke the network. The default is 15.

### Examples

Download images from the top 100 posts in /r/funny and /r/pics into the folder ```Images```, with 10 threads, and only static media:
```
redditset.py --folder images/ --static --subreddits funny pics --limit maxthreads 10 --limmit 100
```

Download images from the top 1000 posts in /r/cars and set the image file name to be the name of the reddit post, with 20 threads
```
redditset.py --folder images/  --subreddits cars --limit maxthreads 20
--limmit 1000 --keep
```


## Dataset

Also included is a script to take a folder with subfolders containing images downloaded with reddit set and creates a Tensorflow dataset out of it, with images resized to 28x28. 

Example
```
import dataset as dataset

valdiationDataset = dataset.createDataset('Images',True,0,.2)
trainingDataset = dataset.createDataset('Images',True,.2,.8)
```

where ```Images/``` Looks like this
```
Images/
  dogs/
    image1.png
    image2.png
  cats/
    image2.png
    image3.png
```

See the file for how it works