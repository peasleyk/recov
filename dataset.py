import os
import tensorflow as tf

def getFiles(folder):
  """
    Takes in a folder
    Returns relative filenames from a folder
  """
  filenames = os.listdir(folder)
  filenames = [os.path.join(folder,x) for x in filenames]
  return filenames

def getFilesAll(folder):
  """
    Inputs:
      folder - A folder with subfolders containing images, where the subfolder name is the class of the image

    Outputs:
      filenames - a list of every filename from each subfolder
      labels - a list of labels, where label[i] is the class of image [i]
  """

  filenames = []
  labels = []
  folders = getFolders(folder)
  for x,folder in enumerate(folders):
    files = getFiles(folder)
    filenames+=files
    # We just want the base folder it's in, as that's the class it is
    labels+= [x] * len(files)

  return filenames,labels

def getFilesSpecific(folder):
  """
    inputs:
      folder - a folder containing images, where the folder name is the class of the images

    outputs:
      filesnames - a list of filenames
      label - a list of the foldername, len(filenames) times
    """

  filenames = getFiles(folder)
  labels = [folder] * len(filenames)
  return filenames, labels


def getFolders(folder):
  """
    Returns subfolders of a parent folder
  """

  folders =  [x[0] for x in os.walk(folder)]
  return folders[1:]

def resizeImage(image,label):
  """
    Takes in an image and label, and returns the preprocessed and shrunk training image
  """

  imageString = tf.read_file(image)
  imageDecoded= tf.image.decode_jpeg(imageString)
  imageResize = tf.image.resize_images(imageDecoded,[28,28])
  return imageResize,label

def createDataset(folder,all=True,lower=0,upper=1):
  """
    Takes in a folder of images, returns a testing dataset of images where folder is structured
    Folder
      dogs
        -image1
        -image2
      cats
        -image2
        -image3

    Where each image is the class of the name of the subdir folder
    see https://www.tensorflow.org/programmers_guide/datasets# decoding_image_data_and_resizing_it

    todo - dataset.batch
  """
  if all:
    filenames,labels = getFilesAll(folder)
  else:
    filenames,labels = getFilesSpecific(folder)

  filenames = filenames[int(lower*len(filenames)):int(upper*len(filenames))]
  labels = labels[int(lower*len(labels)):int(upper*len(labels))]
  dataset = tf.data.Dataset.from_tensor_slices((tf.constant(filenames),tf.constant(labels)))
  dataset = dataset.map(resizeImage)
  dataset = dataset.shuffle(buffer_size=10000)
  return dataset