import os
import sys
import logging
import yaml
import praw
import argparse
import prawcore
import shutil
import requests
import re
import time
import tqdm
from  collections import OrderedDict, defaultdict
# dummy as we're i/o bounded
from multiprocessing.dummy import Pool as ThreadPool

class downit():
  def __init__(self, config, limit, folder,
                keep, threadLimit, dynamic, subreddits):
    self.config = config
    self.logger = logging.getLogger('recov')

    # reddit specific
    self.username = self.config["recov"]["username"]
    self.password = self.config["recov"]["password"]
    self.client_secret = self.config["recov"]["client_secret"]
    self.user_agent = self.config["recov"]["user_agent"]
    self.client_id = self.config["recov"]["client_id"]
    self.reddit = praw.Reddit(client_id=self.client_id,
                                client_secret=self.client_secret,
                                password=self.password,
                                user_agent=self.user_agent,
                                username=self.username)
    self.subreddits = subreddits
    self.limit = limit
    self.folder = folder
    self.keep = keep
    self.threadLimit = threadLimit
    self.dynamic = dynamic

    self.session = requests.Session()
    # Very helpful and underrated SO answer - goo.gl/AzM832
    self.session.mount('https://',
                       requests.adapters.HTTPAdapter(pool_block=False, pool_connections=self.threadLimit,
                                                     pool_maxsize=self.threadLimit))

  def test(self):
    """
      Tests our reddit credentials and keys by trying to get our username
      if this fails, your config.yml or reddit crdentials may be wrong
    """
    try:
      logging.info('Current user is - {}, credentials correct'.format(self.reddit.user.me()))
    except Exception as e:
      logging.error('Problem, {}'.format(e))

  def saveToFolder(self, imageURL, parentFolder, filename):
    """
    Creates the parentFolder if it doesn't exist, saving the image
    there under the filename. ignores duplicates
    """
    if not os.path.exists(parentFolder):
      try:
        os.makedirs(parentFolder)
      except FileExistsError:
        # Two threads try making the dir at the same time,
        # simply ignore the second attempt
        pass
    fullPath = os.path.join(parentFolder, filename)
    if os.path.exists(fullPath):
      logging.error("Already downloaded {}".format(filename))
      return
    response = self.session.get(imageURL, stream=True)
    with open(fullPath, 'wb') as f:
      try:
        shutil.copyfileobj(response.raw, f)
      except shutil.Error:
        # Somehow passed our first check.
        logging.error("Already downloaded {}".format(imageName))
        pass

  def getImgurAlbum(self,URL):
    """
    If the link in a post is an album, grab all the images from
    that album
    """
    try:
      response = self.session.get(URL, stream=True)
    except requests.exceptions.TooManyRedirects:
      return None

    # From https://github.com/alexgisby/imgur-album-downloader/blob/master/LICENSE
    # Looking for <img or an actualy imgur URL i.imgur.com will only return
    # the first 9 images in an album as imgure loads as it scrolls
    # Looking for the hash entries will return every image in the album
    imageList = []
    images = re.findall('.*?{"hash":"([a-zA-Z0-9]+)".*?"ext":"(\.[a-zA-Z0-9]+)".*?', response.text)
    for image in images:
      image = "{}{}".format(image[0], image[1])
      imageList.append("https://i.imgur.com/{}".format(image))

    return list(OrderedDict.fromkeys(imageList))

  def getImageURL(self, URL):
    """
    Returns a valid image url from a reddit post
    Filters static and dynamic urls
    Returns a list if url is to an album
    """
    static = ["png","jpg"]
    dynamic = ["gifv", "webm", "gif"]
    fullImages = "/layout/blog"

    if "https://gfycat.com/" in URL:
      image = URL.split('/')[-1]
      URL = "https://giant.gfycat.com/{}.webm".format(image)
    if "i.reddituploads.com/" in URL:
      return [URL]
    if "i.reddituploads.com/" in URL:
      return [URL]
    if "?1" in URL:
      return [URL.rstrip("?")]
    if "//imgur.com/" in URL:
      return self.getImgurAlbum(URL+fullImages)
    if "m.imgur.com" in URL:
      return  self.getImgurAlbum(URL+fullImages)
    # Known broken host
    if "i.sli.mg" in URL:
      return None

    # If not a special case, look for extension
    end = URL.split('.')[-1]
    if self.dynamic:
      if end in static+dynamic:
        return [URL]
    else:
      if end in static:
        return [URL]

    # If we hit this, we ignore the post
    return None

  def getSubmissions(self):
    """
    Returns a default dict of submissions for each subreddit
    Each entry in the list is tuple of
      - image url
      - folder to save to
      - new filename if we're keeping folders
    """
    submissionsList = defaultdict(list)
    for subreddit in self.subreddits:
      try:
        self.reddit.subreddits.search_by_name(subreddit, exact=True)
      except prawcore.exceptions.NotFound:
        logging.error("Subreddit {} not found".format(subreddit))
        continue

      logging.info("Getting valid images from {}".format(subreddit))
      sub = self.reddit.subreddit(subreddit)
      for submission in sub.top(limit=self.limit):
        location = self.folder+subreddit+'/'
        imageURL = submission.url
        imageList = self.getImageURL(imageURL)
        if imageList:
          for x, image in enumerate(imageList):
            filename = os.path.basename(image)
            if self.keep:
              location= "{}/{}".format(location, submission.title)
            submissionsList[subreddit].append((image, location, filename))
    return submissionsList

  def download(self, imageDict):
    """
    Runs though the dict we collected of posts, creating a
    threadpool for them and starts the pull
    """

    def threadfunc(entryTuple):
      """
      Grabs a single post and downloads the image
      """
      imageURL = entryTuple[0]
      parentFolder = entryTuple[1]
      filename = entryTuple[2]
      try:
        self.saveToFolder(imageURL, parentFolder, filename)
      except OSError as e:
        logging.error("OS error, {}".format(e))
      progress.update(1)

    for key in imageDict.keys():
      totalImages = imageDict[key]
      count = len(totalImages)
      logging.info("Starting {} downloads for {}".format(count, key))
      pool = ThreadPool(self.threadLimit)
      progress = tqdm.tqdm(total=count)
      pool.map(threadfunc, totalImages)
      pool.close()
      pool.join()
      progress.close()

def testsConfig(config):
  """
    Makes sure config.yml is filled out
  """
  for toplevel in config:
    for sublevel in config[toplevel].keys():
      if not config[toplevel][sublevel]:
        logging.error("Please fill out %s field in config" %sublevel)
        sys.exit()

def main(argv):
  config = yaml.safe_load(open("config.yml"))
  testsConfig(config)
  logger = logging.getLogger('com')
  logging.basicConfig(format="%(levelname)s: %(message)s",
                      level=config["logging"]["level"])

  parser = argparse.ArgumentParser()
  parser.add_argument("-t", "--test",
                      help="Tests your reddit connection",
                      action='store_true')
  parser.add_argument("-l", "--limit", type=int, default=500,
                      help="The number of reddit posts to download")
  parser.add_argument("-f", "--folder", default="Images/",
                      help="folder to download images")
  parser.add_argument("-s", "--subreddits", nargs='+',
                      help="list of subreddits to download from")
  parser.add_argument("-k", "--keep",
                      help="Places each posts media in a folder with the submission name",
                      action='store_true')
  parser.add_argument("-m", "--maxthreads", default=15,
                      help="Number of threads to use, images to download at the same time" \
                      "and more dropped connections", type=int)
  parser.add_argument("-d", "--dynamic",
                      help="Allow dynamic media (gifs, mp4s)",
                      action='store_true')

  parser.parse_args()
  args = vars(parser.parse_args())
  limit = args['limit']
  folder = args['folder']
  keep = args['keep']
  threadLimit = args['maxthreads']
  dynamic = args['dynamic']
  subreddits = args['subreddits']
  ro = downit(config, limit, folder, keep, threadLimit, dynamic, subreddits)

  if args['test']:
    ro.test()
  elif args["subreddits"]:
    submissionsList = ro.getSubmissions()
    ro.download(submissionsList)
  else:
    logging.error("At least one subreddit needed")


if __name__ == "__main__":
  try:
    start_time = time.time()
    main(sys.argv[1:])
    print("Total time = {} seconds".format(time.time()-start_time))
  except KeyboardInterrupt:
    sys.exit()
